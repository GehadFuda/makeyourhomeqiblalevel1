import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { HomePage } from './home/home.page';
import { AudioService } from './audio.service';
import { DataService } from './data.service';


@NgModule({
  declarations: [AppComponent, HomePage],
  entryComponents: [HomePage],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    ScreenOrientation,
    AudioService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
