

export class DataService {

    private suraList  = [
        {id: 50, name: 'ق', endPage: 14, numOfAya: 45, type: 'مكية', endAya: 2, numOfJuza: '26'},
        {id: 51, name: 'الذاريات', endPage: 12, numOfAya: 60, type: 'مكية', endAya: 4, numOfJuza: '26'},
        {id: 52, name: 'الطور', endPage: 12, numOfAya: 49, type: 'مكية', endAya: 2, numOfJuza: '27'},
        {id: 53, name: 'النجم', endPage: 13, numOfAya: 62, type: 'مكية', endAya: 3, numOfJuza: '27'},
        {id: 54, name: 'القمر', endPage: 13, numOfAya: 55, type: 'مكية', endAya: 5, numOfJuza: '27'},
        {id: 55, name: 'الرحمن', endPage: 15, numOfAya: 78, type: 'مدنية', endAya: 2, numOfJuza: '27'},
        {id: 56, name: 'الواقعة', endPage: 14, numOfAya: 96, type: 'مدنية', endAya: 2, numOfJuza: '27'},
        {id: 57, name: 'الحديد', endPage: 23, numOfAya: 29, type: 'مدنية', endAya: 1, numOfJuza: '27'},
        {id: 58, name: 'المجادلة', endPage: 18, numOfAya: 22, type: 'مدنية', endAya: 1, numOfJuza: '28'},
        {id: 59, name: 'الحشر', endPage: 19, numOfAya: 24, type: 'مدنية', endAya: 1, numOfJuza: '28'},
        {id: 60, name: 'الممتحنة', endPage: 12, numOfAya: 13, type: 'مدنية', endAya: 2, numOfJuza: '28'},
        {id: 61, name: 'الصف', endPage: 8, numOfAya: 14, type: 'مدنية', endAya: 1, numOfJuza: '28'},
        {id: 62, name: 'الجمعة', endPage: 7, numOfAya: 11, type: 'مدنية', endAya: 1, numOfJuza: '28'},
        {id: 63, name: 'المنافقون', endPage: 8, numOfAya: 11, type: 'مدنية', endAya: 2, numOfJuza: '28'},
        {id: 64, name: 'التغابن', endPage: 11, numOfAya: 18, type: 'مدنية', endAya: 2, numOfJuza: '28'},
        {id: 65, name: 'الطلاق', endPage: 12, numOfAya: 12, type: 'مدنية', endAya: 1, numOfJuza: '28'},
        {id: 66, name: 'التحريم', endPage: 10, numOfAya: 12, type: 'مدنية', endAya: 1, numOfJuza: '28'},
        {id: 67, name: 'الملك', endPage: 12, numOfAya: 30, type: 'مكية', endAya: 1, numOfJuza: '29'},
        {id: 68, name: 'القلم', endPage: 10, numOfAya: 52, type: 'مكية', endAya: 3, numOfJuza: '29'},
        {id: 69, name: 'الحاقة', endPage: 9, numOfAya: 52, type: 'مكية', endAya: 3, numOfJuza: '29'},
        {id: 70, name: 'المعارج', endPage: 7, numOfAya: 44, type: 'مكية', endAya: 5, numOfJuza: '29'},
        {id: 71, name: 'نوح', endPage: 9, numOfAya: 28, type: 'مكية', endAya: 1, numOfJuza: '29'},
        {id: 72, name: 'الجن', endPage: 10, numOfAya: 28, type: 'مكية', endAya: 1, numOfJuza: '29'},
        {id: 73, name: 'المزمل', endPage: 7, numOfAya: 20, type: 'مكية', endAya: 1, numOfJuza: '29'},
        {id: 74, name: 'المدثر', endPage: 8, numOfAya: 56, type: 'مكية', endAya: 5, numOfJuza: '29'},
        {id: 75, name: 'القيامة', endPage: 5, numOfAya: 40, type: 'مكية', endAya: 9, numOfJuza: '29'},
        {id: 76, name: 'الإنسان', endPage: 9, numOfAya: 31, type: 'مدنية', endAya: 1, numOfJuza: '29'},
        {id: 77, name: 'المرسلات', endPage: 8, numOfAya: 50, type: 'مكية', endAya: 2, numOfJuza: '29'},
        // {id: 78, name: 'النبأ', endPage: 7, numOfAya: 40, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 79, name: 'النازعات', endPage: 6, numOfAya: 46, type: 'مكية', endAya: 6, numOfJuza: '30'},
        // {id: 80, name: 'عبس', endPage: 5, numOfAya: 42, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 81, name: 'التكوير', endPage: 4, numOfAya: 29, type: 'مكية', endAya: 4, numOfJuza: '30'},
        // {id: 82, name: 'الانفطار', endPage: 4, numOfAya: 19, type: 'مكية', endAya: 1, numOfJuza: '30'},
        // {id: 83, name: 'المطففين', endPage: 6, numOfAya: 36, type: 'مكية', endAya: 5, numOfJuza: '30'},
        // {id: 84, name: 'الانشقاق', endPage: 4, numOfAya: 25, type: 'مكية', endAya: 7, numOfJuza: '30'},
        // {id: 85, name: 'البروج', endPage: 5, numOfAya: 22, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 86, name: 'الطارق', endPage: 3, numOfAya: 17, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 87, name: 'الأعلى', endPage: 3, numOfAya: 19, type: 'مكية', endAya: 7, numOfJuza: '30'},
        // {id: 88, name: 'الغاشية', endPage: 4, numOfAya: 26, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 89, name: 'الفجر', endPage: 6, numOfAya: 30, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 90, name: 'البلد', endPage: 3, numOfAya: 20, type: 'مكية', endAya: 8, numOfJuza: '30'},
        // {id: 91, name: 'الشمس', endPage: 3, numOfAya: 15, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 92, name: 'الليل', endPage: 3, numOfAya: 21, type: 'مكية', endAya: 5, numOfJuza: '30'},
        // {id: 93, name: 'الضحى', endPage: 2, numOfAya: 11, type: 'مكية', endAya: 7, numOfJuza: '30'},
        // {id: 94, name: 'الشرح', endPage: 2, numOfAya: 8, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 95, name: 'التين', endPage: 2, numOfAya: 8, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 96, name: 'العلق', endPage: 3, numOfAya: 19, type: 'مكية', endAya: 5, numOfJuza: '30'},
        // {id: 97, name: 'القدر', endPage: 2, numOfAya: 5, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 98, name: 'البينة', endPage: 4, numOfAya: 8, type: 'مدنية', endAya: 1, numOfJuza: '30'},
        // {id: 99, name: 'الزلزلة', endPage: 2, numOfAya: 8, type: 'مدنية', endAya: 4, numOfJuza: '30'},
        // {id: 100, name: 'العاديات', endPage: 2, numOfAya: 11, type: 'مكية', endAya: 6, numOfJuza: '30'},
        // {id: 101, name: 'القارعة', endPage: 2, numOfAya: 11, type: 'مكية', endAya: 7, numOfJuza: '30'},
        // {id: 102, name: 'التكاثر', endPage: 2, numOfAya: 8, type: 'مكية', endAya: 4, numOfJuza: '30'},
        // {id: 103, name: 'العصر', endPage: 1, numOfAya: 3, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 104, name: 'الهمزة', endPage: 2, numOfAya: 9, type: 'مدنية', endAya: 5, numOfJuza: '30'},
        // {id: 105, name: 'الفيل', endPage: 2, numOfAya: 5, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 106, name: 'قريش', endPage: 2, numOfAya: 4, type: 'مكية', endAya: 1, numOfJuza: '30'},
        // {id: 107, name: 'الماعون', endPage: 2, numOfAya: 7, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 108, name: 'الكوثر', endPage: 1, numOfAya: 3, type: 'مكية', endAya: 3, numOfJuza: '30'},
        // {id: 109, name: 'الكافرون', endPage: 2, numOfAya: 6, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 110, name: 'النصر', endPage: 2, numOfAya: 3, type: 'مدنية', endAya: 1, numOfJuza: '30'},
        // {id: 111, name: 'المسد', endPage: 2, numOfAya: 5, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 112, name: 'الإخلاص', endPage: 1, numOfAya: 4, type: 'مكية', endAya: 4, numOfJuza: '30'},
        // {id: 113, name: 'الفلق', endPage: 2, numOfAya: 5, type: 'مكية', endAya: 2, numOfJuza: '30'},
        // {id: 114, name: 'الناس', endPage: 2, numOfAya: 6, type: 'مكية', endAya: 2, numOfJuza: '30'},
      ];

    constructor() {}

    getSuraList() {
        return this.suraList;
    }

    getSuraInfo(id): object {
        let suraInfo;
        this.suraList.filter((value, index) => {
            if (id == value.id) {
                suraInfo =  value;
            }
        });

        return suraInfo;
    }

}