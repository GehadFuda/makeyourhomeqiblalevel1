import { Component } from '@angular/core';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Platform } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { AudioService } from '../audio.service';
import { Subscription } from 'rxjs';
import { DataService } from '../data.service';

@Component({
  selector: 'app-sura',
  templateUrl: './sura.page.html',
  styleUrls: ['./sura.page.scss'],
})
export class SuraPage {

  public status = false;
  public currentSura: any;
  public suraPage: any;
  public durationxx = 0;
  public currentAya = 0;
  public currentPage = 1;
  public showingImg = false;
  public isEnded: Subscription;
  public canPaused = false;
  public canMove = true;
  public repeat = true;
  public suraInfo: object;

  constructor(private screenOrientation: ScreenOrientation,
              private platform: Platform,
              private activatedRoute: ActivatedRoute,
              public audio: AudioService,
              private data: DataService) {


    this.audio.isEnded.subscribe((msg) => {
      if (this.currentPage == this.suraInfo['endPage'] && this.currentAya == this.suraInfo['endAya']) {
        if (this.repeat == false) {
          return;
        }else if (this.repeat = true) {
          this.goToTheStartSura();
        }
      }
      this.currentAya++; // 12 ==12
      this.audio.play('assets/sound/' + this.currentSura + '/page' + this.currentPage + 'aya' + this.currentAya + '.mp3');
      this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;

      setTimeout(() => {
        if (isNaN(audio.audioObj.duration)) {
          this.currentPage++;
          this.currentAya = 1;
          this.audio.play('assets/sound/' + this.currentSura + '/page' + this.currentPage + 'aya' + this.currentAya + '.mp3');
          this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
        }
      }, 500);
    });

  }

  lockScreenOrientation() {
    try {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    } catch (error) {
      console.error(error);
    }
  }

  ionViewDidEnter() {
    this.activatedRoute.paramMap.subscribe(param => {
      this.currentSura = param.get('id');
      this.suraInfo = this.data.getSuraInfo(this.currentSura);
    });

    this.suraPage = 'page1aya0';

    if (this.platform.is('cordova')) {
      this.lockScreenOrientation();
    }
  }

  play() {
    this.canPaused = true;
    this.canMove = false;
    this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    this.audio.play('assets/sound/' + this.currentSura + '/page' + this.currentPage + 'aya' + this.currentAya + '.mp3');
  }

  stop() {
    this.audio.stop();
    this.currentAya = 1;
    if (this.currentPage == 1) {
      this.currentAya = 0;
    }
    // this.currentPage = 1;
    this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    this.canPaused = false;
    this.canMove = true;
  }

  pause() {
    this.canPaused = false;
    this.canMove = false;
    this.audio.pause();
  }

  next() {
    if (this.canMove && !this.showingImg) {
      if (this.currentPage + 1 > this.suraInfo['endPage']) {
        return;
      }
      this.currentPage++;
      this.currentAya = 1;
      this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    }
  }

  prev() {
    if (this.canMove && !this.showingImg) {
      if (this.currentPage == 1) {
        return;
      }
      this.currentPage--;
      if (this.currentPage == 1) {
        this.currentAya = 0;
      } else {
        this.currentAya = 1;
      }
      // this.audio.play('assets/sound/page' + this.currentPage + 'aya' + this.currentAya + '.mp3');
      this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    }
  }

  showMindMap() {
    this.showingImg = !this.showingImg;
  }

  repeatSura() {
    if(this.repeat) {
      this.repeat = false;
    }else {
      this.repeat = true;
    }
  }

  goToTheStartSura() {
    this.currentPage = 1;
    this.currentAya = -1;
    this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    // if (this.canMove && !this.showingImg) {
      
    // }
  }

  goToTheNextSura() {
    if (this.canMove && !this.showingImg) {
      this.currentPage = 1;
      this.currentAya = 0;
      if (this.currentSura == 77) {
        this.currentSura = 50;
      } else {
        this.currentSura++;
      }
      this.suraInfo = this.data.getSuraInfo(this.currentSura);
      this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    }
  }

  goToThePreviousSura() {
    if (this.canMove && !this.showingImg) {
      this.currentPage = 1;
      this.currentAya = 0;
      if (this.currentSura == 50) {
        this.currentSura = 77;
      } else {
        this.currentSura--;
      }
      this.suraInfo = this.data.getSuraInfo(this.currentSura);
      this.suraPage = 'page' + this.currentPage + 'aya' + this.currentAya;
    }
  }

  goTo(suraPage: string) {
    this.canPaused = true;
    this.canMove = false;
    this.audio.play('assets/sound/' + this.currentSura + '/' + suraPage + '.mp3');
  }
}
