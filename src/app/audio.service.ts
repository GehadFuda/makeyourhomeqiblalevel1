import {Injectable, Output, EventEmitter} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import * as moment from 'moment';

@Injectable()
export class AudioService {
  private stop$ = new Subject();
  public audioObj = new Audio('');
  public audioCheck = new Subject();
  public isPaused = false;
  public pausedTime = 0;

  public isEnded = new Subject<string>();
  constructor() { 
    this.checkIfEnded();
  }

  private streamObservable(url) {
    const events = [
      'ended', 'error', 'play', 'playing', 'pause', 'timeupdate', 'canplay', 'loadedmetadata', 'loadstart'
    ];

    const addEvents = function(obj, events, handler) {
      events.forEach(event => {
        obj.addEventListener(event, handler);
      });
    };

    const removeEvents = function(obj, events, handler) {
      events.forEach(event => {
        obj.removeEventListener(event, handler);
      });
    };

    return Observable.create(observer => {
      // Play audio
      this.audioObj.src = url;
      this.audioObj.load();
      this.audioObj.play();

      // Media Events
      const handler = (event) => observer.next(event);
      addEvents(this.audioObj, events, handler);

      return () => {
        // Stop Playing
        this.audioObj.pause();
        this.audioObj.currentTime = 0;

        // Remove EventListeners
        removeEvents(this.audioObj, events, handler);
      };
    });
  }


  playStream(url) {
    return this.streamObservable(url).pipe(takeUntil(this.stop$));
  }
  play(src) {
    if (this.isPaused) {
      this.audioObj.currentTime = this.pausedTime;
      // this.audioObj.load();
      this.audioObj.play();
      this.isPaused = false;
    }else {
      this.audioObj.src = src;
      this.audioObj.play();
    }
  }

  pause() {
    this.isPaused = true;
    this.pausedTime = this.audioObj.currentTime;
    this.audioObj.pause();
    // this.audioObj.currentTime = 0;
  }

  stop() {
    this.audioObj.currentTime = 0;
    this.isPaused = false;
    this.audioObj.pause();
  }

  seekTo(seconds) {
    this.audioObj.currentTime = seconds;
  }

  formatTime(time, format) {
    return moment.utc(time).format(format);
  }

  getCurrentTime() {
    return this.audioObj.currentTime;
  }

  getDuration() {
    return this.audioObj.duration;
  }

  checkIfEnded() {
  this.audioObj.addEventListener('ended', () => {
    this.isEnded.next("Done");
  });
  }
}
