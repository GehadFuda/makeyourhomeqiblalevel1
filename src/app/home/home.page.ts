import { Component } from '@angular/core';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Platform } from '@ionic/angular';

import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public suraList = [];
  constructor(private router: Router,
              private screenOrientation: ScreenOrientation,
              private platform: Platform, private data:DataService) {
                this.suraList = this.data.getSuraList();
                console.log(this.suraList)
              }

    lockScreenOrientation() {
      try {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      } catch (error) {
        console.error(error);
      }
    }

    ionViewDidEnter() {
      if (this.platform.is('cordova')) {
        this.lockScreenOrientation();
      }
    }

    goToSura(id) {
      this.router.navigate(['/sura', id]);
    }

}
